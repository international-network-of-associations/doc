= Documentation RIA
:imagesdir: docs/images/

== Aruba

Suite à des problèmes avec les switchs Aruba CX visiblement lié aux LAG multi-chassis et trop long à résoudre nous décider de les remplacer par des routeurs mikrotik.

Une documentation sur Aruba reste disponible ici : link:docs/Switchs_Aruba.adoc[].

Nous sommes déçu de devoir se séparer de ces équipements vu le temps que nous avons passé à les configurer. Vous trouverez les fichiers de configuration ici : link:https://gitlab.com/international-network-of-associations/switchs_configs/[].

== Fortigate

Pour les pare-feux vous trouverez nos recherches par rapport à l´automatisation dans le dépôt https://gitlab.com/international-network-of-associations/network_conf[network_conf].
Suite à de nombreux problèmes ce dépôt n´est plus utilisé et le déploiement est manuel pour ces équipements.

=== Déploiement rapide

Afin de redéployer les fortigates à partir de 2 pare-feux en HA vous pouvez restaurer la configuration se trouvant dans le dépôt https://gitlab.com/international-network-of-associations/confs/-/tree/master/RIA/Firewalls[Confs]; il est également possible de copier coller le contenu de ces fichiers dans le terminal du pare-feu "Primaire".

.HA sur les Fortigate
image::forti_ha.png[]

== Routeurs

Pour le déploiement des routeurs Mikrotik deux méthode de restauration sont possibles.

Les fichiers sont disponibles dans le dépôt https://gitlab.com/international-network-of-associations/confs/-/tree/master/RIA/Routers[Confs].

- Via Winbox (l'outil de gestion de mikrotik) il est possible d'ajouter le fichier de backup depuis l´onglet file et de le restaurer.
- Directement dans le terminal vous pouvez coller les scripts qui se trouvent dans le dossier https://gitlab.com/international-network-of-associations/confs/-/tree/master/RIA/Routers/scripts[]. _Ces fichiers étant en clair ils sont vide de toutes données sensibles (commande : `export hide-sentives`) comme les mots de passes._ 

.Restauration via Windbox

image::https://i1.wp.com/systemzone.net/wp-content/uploads/2018/07/MikroTik-Backup-Configuration-File.jpg[]